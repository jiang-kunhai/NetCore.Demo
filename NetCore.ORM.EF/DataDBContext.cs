﻿using Microsoft.EntityFrameworkCore;
using NetCore.ORM.EF.PO;

namespace NetCore.ORM.EF
{
    public partial class DataDBContext : BaseDbContext
    {
        /// <summary>
        /// 用户
        /// </summary>
        public DbSet<User> User { get; set; }
    }
}