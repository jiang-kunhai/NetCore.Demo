//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Server")]
    public class Server
    {

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Id", TypeName = "nvarchar", Order = 0)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		[Required]
		[MaxLength(200)]
        public string Id { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Data", TypeName = "nvarchar")]
        public string Data { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("LastHeartbeat", TypeName = "datetime")]
		[Required]
        public DateTime LastHeartbeat { get; set; }

		/// <summary>
        /// Server对象的属性名称
        /// </summary>
		public ServerProperties PropertyNames { get; } = new ServerProperties();
    }
	
    /// <summary>
    /// Server对象的属性名称
    /// </summary>
	public class ServerProperties
    {
		public string Id { get; } = "Id";
		public string Data { get; } = "Data";
		public string LastHeartbeat { get; } = "LastHeartbeat";
	}
}
