//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Hash")]
    public class Hash
    {

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Key", TypeName = "nvarchar", Order = 0)]
		[Required]
		[MaxLength(100)]
        public string Key { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Field", TypeName = "nvarchar", Order = 1)]
		[Required]
		[MaxLength(100)]
        public string Field { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Value", TypeName = "nvarchar")]
        public string Value { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("ExpireAt", TypeName = "datetime2")]
        public DateTime? ExpireAt { get; set; }

		/// <summary>
        /// Hash对象的属性名称
        /// </summary>
		public HashProperties PropertyNames { get; } = new HashProperties();
    }
	
    /// <summary>
    /// Hash对象的属性名称
    /// </summary>
	public class HashProperties
    {
		public string Key { get; } = "Key";
		public string Field { get; } = "Field";
		public string Value { get; } = "Value";
		public string ExpireAt { get; } = "ExpireAt";
	}
}
