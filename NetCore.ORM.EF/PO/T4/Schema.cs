//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Schema")]
    public class Schema
    {

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Version", TypeName = "int", Order = 0)]
		[Required]
        public int Version { get; set; }

		/// <summary>
        /// Schema对象的属性名称
        /// </summary>
		public SchemaProperties PropertyNames { get; } = new SchemaProperties();
    }
	
    /// <summary>
    /// Schema对象的属性名称
    /// </summary>
	public class SchemaProperties
    {
		public string Version { get; } = "Version";
	}
}
