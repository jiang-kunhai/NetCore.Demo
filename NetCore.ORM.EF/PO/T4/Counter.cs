//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Counter")]
    public class Counter
    {

		/// <summary>
        /// 
        /// </summary>
		[Column("Key", TypeName = "nvarchar")]
		[Required]
		[MaxLength(100)]
        public string Key { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Value", TypeName = "int")]
		[Required]
        public int Value { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("ExpireAt", TypeName = "datetime")]
        public DateTime? ExpireAt { get; set; }

		/// <summary>
        /// Counter对象的属性名称
        /// </summary>
		public CounterProperties PropertyNames { get; } = new CounterProperties();
    }
	
    /// <summary>
    /// Counter对象的属性名称
    /// </summary>
	public class CounterProperties
    {
		public string Key { get; } = "Key";
		public string Value { get; } = "Value";
		public string ExpireAt { get; } = "ExpireAt";
	}
}
