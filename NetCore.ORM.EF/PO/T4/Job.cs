//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Job")]
    public class Job
    {

		/// <summary>
        /// 
        /// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key, Column("Id", TypeName = "bigint", Order = 0)]
		[Required]
        public long Id { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("StateId", TypeName = "bigint")]
        public long? StateId { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("StateName", TypeName = "nvarchar")]
		[MaxLength(20)]
        public string StateName { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("InvocationData", TypeName = "nvarchar")]
		[Required]
        public string InvocationData { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Arguments", TypeName = "nvarchar")]
		[Required]
        public string Arguments { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("CreatedAt", TypeName = "datetime")]
		[Required]
        public DateTime CreatedAt { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("ExpireAt", TypeName = "datetime")]
        public DateTime? ExpireAt { get; set; }

		/// <summary>
        /// Job对象的属性名称
        /// </summary>
		public JobProperties PropertyNames { get; } = new JobProperties();
    }
	
    /// <summary>
    /// Job对象的属性名称
    /// </summary>
	public class JobProperties
    {
		public string Id { get; } = "Id";
		public string StateId { get; } = "StateId";
		public string StateName { get; } = "StateName";
		public string InvocationData { get; } = "InvocationData";
		public string Arguments { get; } = "Arguments";
		public string CreatedAt { get; } = "CreatedAt";
		public string ExpireAt { get; } = "ExpireAt";
	}
}
