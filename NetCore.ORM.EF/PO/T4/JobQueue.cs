//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("JobQueue")]
    public class JobQueue
    {

		/// <summary>
        /// 
        /// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key, Column("Id", TypeName = "bigint", Order = 0)]
		[Required]
        public long Id { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("JobId", TypeName = "bigint")]
		[Required]
        public long JobId { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Queue", TypeName = "nvarchar", Order = 1)]
		[Required]
		[MaxLength(50)]
        public string Queue { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("FetchedAt", TypeName = "datetime")]
        public DateTime? FetchedAt { get; set; }

		/// <summary>
        /// JobQueue对象的属性名称
        /// </summary>
		public JobQueueProperties PropertyNames { get; } = new JobQueueProperties();
    }
	
    /// <summary>
    /// JobQueue对象的属性名称
    /// </summary>
	public class JobQueueProperties
    {
		public string Id { get; } = "Id";
		public string JobId { get; } = "JobId";
		public string Queue { get; } = "Queue";
		public string FetchedAt { get; } = "FetchedAt";
	}
}
