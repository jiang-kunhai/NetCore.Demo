//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("JobParameter")]
    public class JobParameter
    {

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("JobId", TypeName = "bigint", Order = 0)]
		[Required]
        public long JobId { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Name", TypeName = "nvarchar", Order = 1)]
		[Required]
		[MaxLength(40)]
        public string Name { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Value", TypeName = "nvarchar")]
        public string Value { get; set; }

		/// <summary>
        /// JobParameter对象的属性名称
        /// </summary>
		public JobParameterProperties PropertyNames { get; } = new JobParameterProperties();
    }
	
    /// <summary>
    /// JobParameter对象的属性名称
    /// </summary>
	public class JobParameterProperties
    {
		public string JobId { get; } = "JobId";
		public string Name { get; } = "Name";
		public string Value { get; } = "Value";
	}
}
