//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("State")]
    public class State
    {

		/// <summary>
        /// 
        /// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key, Column("Id", TypeName = "bigint", Order = 0)]
		[Required]
        public long Id { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("JobId", TypeName = "bigint", Order = 1)]
		[Required]
        public long JobId { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Name", TypeName = "nvarchar")]
		[Required]
		[MaxLength(20)]
        public string Name { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Reason", TypeName = "nvarchar")]
		[MaxLength(100)]
        public string Reason { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("CreatedAt", TypeName = "datetime")]
		[Required]
        public DateTime CreatedAt { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Data", TypeName = "nvarchar")]
        public string Data { get; set; }

		/// <summary>
        /// State对象的属性名称
        /// </summary>
		public StateProperties PropertyNames { get; } = new StateProperties();
    }
	
    /// <summary>
    /// State对象的属性名称
    /// </summary>
	public class StateProperties
    {
		public string Id { get; } = "Id";
		public string JobId { get; } = "JobId";
		public string Name { get; } = "Name";
		public string Reason { get; } = "Reason";
		public string CreatedAt { get; } = "CreatedAt";
		public string Data { get; } = "Data";
	}
}
