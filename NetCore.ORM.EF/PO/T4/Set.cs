//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{

    /// <summary>
    /// 
    /// </summary>
	[Table("Set")]
    public class Set
    {

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Key", TypeName = "nvarchar", Order = 0)]
		[Required]
		[MaxLength(100)]
        public string Key { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("Score", TypeName = "float")]
		[Required]
        public double Score { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Key, Column("Value", TypeName = "nvarchar", Order = 1)]
		[Required]
		[MaxLength(256)]
        public string Value { get; set; }

		/// <summary>
        /// 
        /// </summary>
		[Column("ExpireAt", TypeName = "datetime")]
        public DateTime? ExpireAt { get; set; }

		/// <summary>
        /// Set对象的属性名称
        /// </summary>
		public SetProperties PropertyNames { get; } = new SetProperties();
    }
	
    /// <summary>
    /// Set对象的属性名称
    /// </summary>
	public class SetProperties
    {
		public string Key { get; } = "Key";
		public string Score { get; } = "Score";
		public string Value { get; } = "Value";
		public string ExpireAt { get; } = "ExpireAt";
	}
}
