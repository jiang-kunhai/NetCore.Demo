//------------------------------------------------------------------------------
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCore.ORM.EF.PO
{
    /// <summary>
    /// 用户
    /// </summary>
	[Table("tb_user")]
    public class User
    {
        /// <summary>
        /// id
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column("id", TypeName = "bigint", Order = 0)]
        public long Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Column("name", TypeName = "nvarchar")]
        public string Name { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        [Column("birthday", TypeName = "datetime")]
        public DateTime Birthday { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        [Column("age", TypeName = "int")]
        public int Age { get; set; }

        /// <summary>
        /// 性别 1 男 0 女
        /// </summary>
        [Column("sex", TypeName = "bit")]
        public bool Sex { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        [Column("height", TypeName = "float")]
        public double Height { get; set; }

        /// <summary>
        /// 存款
        /// </summary>
        [Column("money", TypeName = "decimal")]
        public decimal? Money { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [Column("address", TypeName = "nvarchar")]
        public string Address { get; set; }

        /// <summary>
        /// User对象的属性名称
        /// </summary>
        public UserProperties PropertyNames { get; } = new UserProperties();
    }

    /// <summary>
    /// User对象的属性名称
    /// </summary>
    public class UserProperties
    {
        public string Id { get; } = "Id";
        public string Name { get; } = "Name";
        public string Birthday { get; } = "Birthday";
        public string Age { get; } = "Age";
        public string Sex { get; } = "Sex";
        public string Height { get; } = "Height";
        public string Money { get; } = "Money";
        public string Address { get; } = "Address";
    }
}