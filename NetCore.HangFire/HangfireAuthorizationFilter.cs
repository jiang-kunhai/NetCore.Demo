﻿using Hangfire.Dashboard;

namespace NetCore.HangFire
{
    /// <summary>
    /// Hangfire仪表盘访问权限控制
    /// </summary>
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Authorize(DashboardContext context)
        {
            if (string.IsNullOrEmpty(context.Request.RemoteIpAddress))
            {
                return false;
            }

            if (context.Request.RemoteIpAddress == "127.0.0.1" || context.Request.RemoteIpAddress == "::1")
            {
                return true;
            }

            if (context.Request.RemoteIpAddress == context.Request.LocalIpAddress)
            {
                return true;
            }

            return false;
        }
    }
}