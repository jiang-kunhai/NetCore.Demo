using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCore.HangFire.Job;
using System;

namespace NetCore.HangFire
{
    public class Startup
    {
        public IConfiguration _configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //添加Hangfire服务
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(_configuration["ConnectionStrings:SqlServer"], new SqlServerStorageOptions()
                {
                    UseRecommendedIsolationLevel = true, // 事务隔离级别。默认是读取已提交。
                    QueuePollInterval = TimeSpan.FromSeconds(15.0),           //- 作业队列轮询间隔。默认值为15秒。
                    JobExpirationCheckInterval = TimeSpan.FromHours(1),       //- 作业到期检查间隔（管理过期记录）。默认值为1小时。
                    CountersAggregateInterval = TimeSpan.FromMinutes(5),      //- 聚合计数器的间隔。默认为5分钟。
                    PrepareSchemaIfNecessary = true,                          //- 如果设置为true，则创建数据库表。默认是true。
                    DashboardJobListLimit = 50000,                            //- 仪表板作业列表限制。默认值为50000。
                    TransactionTimeout = TimeSpan.FromMinutes(1),             //- 交易超时。默认为1分钟。
                    SchemaName = "Hangfire"                                   //- 数据库中表的前缀。
                }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //使用hangfire仪表盘
            app.UseHangfireDashboard(
                "",//设置访问路径
                new DashboardOptions()
                {
                    Authorization = new[]
                    {
                        //new LocalRequestsOnlyAuthorizationFilter(),//仅本地访问
                        new HangfireAuthorizationFilter()//自定义访问权限
                    },
                    DashboardTitle = "Hangfire仪表盘",//仪表盘标题
                }
             );

            //立即执行
            BackgroundJob.Enqueue(() => BingImgJob.GetCurrentImg("立即执行"));
            //延迟任务
            BackgroundJob.Schedule(() => BingImgJob.GetCurrentImg("延迟1分钟执行"), TimeSpan.FromMinutes(1));
            //延续任务
            var id = BackgroundJob.Enqueue(() => BingImgJob.GetCurrentImg("延续任务"));
            BackgroundJob.ContinueJobWith(id, () => BingImgJob.GetCurrentImg($"延续任务(前置任务id-{id})"));
            //CRON表达式执行循环任务，cron在线生成器 https://cron.qqe2.com/ 只能5-6位
            RecurringJob.AddOrUpdate(() => BingImgJob.GetCurrentImg("循环任务"), "0 0 * * * ?", TimeZoneInfo.Local);

            //启用Hangfire服务
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                Queues = new[] { "default" },//配置要处理的队列列表，只能为小写，如果多个服务器同时连接到数据库，会认为是分布式的一份子。
                WorkerCount = Environment.ProcessorCount * 5, //并发任务数，超出并发数。将等待之前任务的完成  (推荐并发线程是cpu 的 5倍)
                ServerName = "本地服务器",//服务器名称
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHangfireDashboard();
            });
        }
    }
}