﻿using NetCore.Identity.Common.ApiScopes;

namespace NetCore.Identity.Common
{
    public class OAuthConfig
    {
        /// <summary>
        /// 过期秒数
        /// </summary>
        public const int ExpireIn = 36000;

        /// <summary>
        /// 用户Api相关
        /// </summary>
        public static class UserApi
        {
            /// <summary>
            /// ID
            /// </summary>
            public static string ApiName = "user_api";

            /// <summary>
            /// api名称
            /// </summary>
            public static string ApiDisplayName = "用户API";

            /// <summary>
            /// 客户端id
            /// </summary>
            public static string ClientId = "user_client_id";

            /// <summary>
            /// 客户端加密方式
            /// </summary>
            public static string Secret = "user_secret";

            /// <summary>
            /// 受保护的api
            /// </summary>
            public static UserApiScopes ApiScopes => new UserApiScopes();
        }
    }
}