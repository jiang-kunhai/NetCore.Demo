﻿namespace NetCore.Identity.Common.ApiScopes.UserApi
{
    /// <summary>
    /// 天气接口
    /// </summary>
    public class Weather
    {
        public string Name = "userapi.weather.scope";

        public string DisplayName = "用户API天气接口";
    }
}