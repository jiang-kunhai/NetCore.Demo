﻿namespace NetCore.Identity.Common.ApiScopes.UserApi
{
    /// <summary>
    /// 测试接口
    /// </summary>
    public class Test
    {
        public string Name = "userapi.test.scope";

        public string DisplayName = "用户API天气接口";
    }
}