﻿using NetCore.Identity.Common.ApiScopes.UserApi;

namespace NetCore.Identity.Common.ApiScopes
{
    /// <summary>
    /// 用户API
    /// </summary>
    public class UserApiScopes
    {
        /// <summary>
        /// 用户API天气接口
        /// </summary>
        public Weather Weather = new Weather();

        /// <summary>
        /// 用户API测试
        /// </summary>
        public Test Test = new Test();
    }
}