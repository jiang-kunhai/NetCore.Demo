using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCore.Identity.Common;

namespace NetCore.Identity.UserApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //指定认证方案
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                //添加Token验证服务到DI
                .AddJwtBearer(options =>
                {
                    //指定授权地址(NetCore.Identity.Server)
                    options.Authority = "http://localhost:5000";
                    //指定访问的api资源，NetCore.Identity.Server中ApiResource的Name
                    options.Audience = OAuthConfig.UserApi.ApiName;
                    //不使用https
                    options.RequireHttpsMetadata = false;
                });

            //授权配置
            services.AddAuthorization(options =>
            {
                //基于策略授权
                options.AddPolicy("WeatherPolicy", builder =>
                {
                    //客户端Scope中包含userapi.weather.scope才能访问
                    builder.RequireScope(OAuthConfig.UserApi.ApiScopes.Weather.Name);
                });
                //基于策略授权
                options.AddPolicy("TestPolicy", builder =>
                {
                    //客户端Scope中包含userapi.test.scope才能访问
                    builder.RequireScope(OAuthConfig.UserApi.ApiScopes.Test.Name);
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //身份验证
            app.UseAuthentication();

            //授权
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}