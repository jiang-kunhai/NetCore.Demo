using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace NetCore.Identity.Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            #region 内存方式

            services.AddIdentityServer()
                //添加证书加密方式，执行该方法，会先判断tempkey.rsa证书文件是否存在，
                //如果不存在的话，就创建一个新的tempkey.rsa证书文件，如果存在的话，就使用此证书文件。
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(Config.IdentityResources)//配置身份资源
                .AddInMemoryApiScopes(Config.ApiScopes)//添加API作用域
                .AddInMemoryApiResources(Config.ApiResources)//配置API资源
                .AddInMemoryClients(Config.Clients)//预置允许验证的客户端
                .AddTestUsers(Config.TestUser);//添加测试的用户

            #endregion 内存方式
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //添加IdentityServer4
            app.UseIdentityServer();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}