﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using NetCore.Identity.Common;
using System.Collections.Generic;
using System.Linq;

namespace NetCore.Identity.Server
{
    public class Config
    {
        /// <summary>
        /// 身份资源，控制身份令牌包含的信息
        /// </summary>
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        /// <summary>
        /// API作用域
        /// </summary>
        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope(OAuthConfig.UserApi.ApiScopes.Weather.Name, OAuthConfig.UserApi.ApiScopes.Weather.DisplayName),
                new ApiScope(OAuthConfig.UserApi.ApiScopes.Test.Name, OAuthConfig.UserApi.ApiScopes.Test.DisplayName),
            };

        /// <summary>
        /// API资源
        /// </summary>
        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource(OAuthConfig.UserApi.ApiName, OAuthConfig.UserApi.ApiDisplayName)
                {
                    //4.X版本增加
                    Scopes = {
                        OAuthConfig.UserApi.ApiScopes.Weather.Name,
                        OAuthConfig.UserApi.ApiScopes.Test.Name,
                    }
                },
            };

        /// <summary>
        /// 客户端配置
        /// </summary>
        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client()
                    {
                        //客户端id
                        ClientId = OAuthConfig.UserApi.ClientId,
                        //配置授权类型，可以配置多个授权类型
                        AllowedGrantTypes = new List<string>()
                        {
                            //授权类型，这里使用的是密码模式ResourceOwnerPassword
                            GrantTypes.ResourceOwnerPassword.FirstOrDefault()
                        },
                        //客户端加密方式
                        ClientSecrets = { new Secret(OAuthConfig.UserApi.Secret.Sha256()) },
                        //配置允许访问的API作用域
                        AllowedScopes = new List<string>(){
                            OAuthConfig.UserApi.ApiScopes.Weather.Name,
                            OAuthConfig.UserApi.ApiScopes.Test.Name,
                        },
                        //配置Token 失效时间，毫秒
                        AccessTokenLifetime = 360000,
                    },
            };

        /// <summary>
        /// 测试用户列表
        /// </summary>
        public static List<TestUser> TestUser =>
            new List<TestUser>()
            {
                new TestUser()
                    {
                         SubjectId = "1",
                         Username = "test",
                         Password = "123456"
                    }
            };
    }
}