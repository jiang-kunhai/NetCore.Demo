﻿using NetCore.Common.Models;
using System;
using System.Collections.Generic;

namespace NetCore.Common.Helpers
{
    /// <summary>
    /// 随机用户对象对象
    /// </summary>
    public class RandomUserHelper
    {
        private static List<UserModel> list;

        public static List<UserModel> GetList()
        {
            if (list != null)
            {
                return list;
            }
            list = new List<UserModel>();
            for (int i = 1; i <= 10000; i++)
            {
                list.Add(new UserModel()
                {
                    Id = i,
                    Name = $"姓名{i}",
                    Birthday = DateTime.Now.AddYears(-(i % 100)),
                    Age = i % 100,
                    Sex = i % 2 == 0,
                    Height = 177.88,
                    Address = $"地址{i}-{Guid.NewGuid().ToString()}{Guid.NewGuid().ToString()}"
                });
            }
            return list;
        }
    }
}