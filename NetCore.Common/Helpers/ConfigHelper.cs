﻿using Microsoft.Extensions.Configuration;

namespace NetCore.Common.Helpers
{
    public class ConfigHelper
    {
        //在Startup中初始化
        public static IConfiguration _configuration;

        public static string GetConfig(string key)
        {
            return _configuration[key];
        }

        public static T GetConfig<T>(string key) where T : new()
        {
            T config = new T();
            _configuration.Bind(key, config);
            return config;
        }
    }
}