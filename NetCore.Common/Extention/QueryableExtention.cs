﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.Linq {

    /// <summary>
    /// IQueryable扩展
    /// </summary>
    public static class QueryableExtention {

        /// <summary>
        /// 在condition为true的情况下应用Where表达式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> source, bool condition, Expression<Func<T, bool>> expression) {
            return condition ? source.Where(expression) : source;
        }

        /// <summary>
        /// 在value不为空字符串的情况下应用Where表达式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereIfNoBlank<T, T1>(this IQueryable<T> source, string value, Expression<Func<T, bool>> expression) {
            if (value == null) {
                return source;
            }
            else if (value.ToString().Trim().Length == 0) {
                return source;
            }
            else {
                return source.Where(expression);
            }
        }
    }
}
