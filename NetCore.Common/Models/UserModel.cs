﻿using System;

namespace NetCore.Common.Models
{
    public class UserModel
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 性别 true 男 false 女
        /// </summary>
        public bool Sex { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// 存款
        /// </summary>
        public decimal? Money { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
    }
}