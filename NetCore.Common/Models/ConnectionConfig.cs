﻿namespace NetCore.Common.Models
{
    /// <summary>
    /// 连接字符串配置
    /// </summary>
    public class ConnectionConfig
    {
        /// <summary>
        /// 配置文件名称
        /// </summary>
        public const string ConfigName = "ConnectionStrings";

        /// <summary>
        /// SqlServer
        /// </summary>
        public string SqlServer { get; set; }
    }
}