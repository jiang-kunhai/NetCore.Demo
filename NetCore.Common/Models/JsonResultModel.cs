﻿namespace NetCore.Common.Models
{
    /// <summary>
    /// json结果返回
    /// </summary>
    public class JsonResultModel
    {
        /// <summary>
        /// 操作是否成功
        /// </summary>
        public bool IsOk { get; set; } = true;

        /// <summary>
        /// 返回信息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 定义的数据
        /// </summary>
        public object Data { get; set; }
    }
}