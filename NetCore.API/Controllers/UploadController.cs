﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NetCore.API.Controllers
{
    public class UploadController : BaseController
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file">文件对象</param>
        /// <returns>文件名称</returns>
        [HttpPost]
        public string File([FromForm] IFormFile file)
        {
            if (file != null)
            {
                return file.FileName;
            }
            return "";
        }
    }
}