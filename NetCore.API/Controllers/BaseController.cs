﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace NetCore.API.Controllers
{
    /// <summary>
    /// 基础控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]//启用验证
    //[Authorize(Roles = "Admin")] // 角色授权
    public class BaseController : ControllerBase
    {
    }
}