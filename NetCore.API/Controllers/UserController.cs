﻿using Microsoft.AspNetCore.Mvc;
using NetCore.Common.Helpers;
using NetCore.Common.Models;
using System.Collections.Generic;
using System.Linq;

namespace NetCore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// 获取值
        /// </summary>
        /// <param name="start">第几行开始</param>
        /// <param name="length">获取几条</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> Get(int start = 0, int length = 10)
        {
            return RandomUserHelper.GetList().Skip(start).Take(length).ToList();
        }

        /// <summary>
        /// 根据id获取值
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<UserModel> Get(int id)
        {
            return RandomUserHelper.GetList().Where(a => a.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// 提交值
        /// </summary>
        /// <param name="value">值</param>
        [HttpPost]
        public void Post([FromBody] UserModel value)
        {
        }

        /// <summary>
        /// 获取值
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="value">值</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UserModel value)
        {
        }

        /// <summary>
        /// 根据id删除值
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}