﻿using Microsoft.AspNetCore.Mvc;
using NetCore.API.Extensions.Swagger;
using NetCore.Common.Models;
using NetCore.ORM.EF.DAO;
using NetCore.ORM.EF.PO;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace NetCore.API.Controllers
{
    /// <summary>
    /// ef-sqlserver
    /// </summary>
    public class EFSqlServerController : BaseController
    {
        /// <summary>
        /// 示例，直接引用dao，就不写BLL层了
        /// </summary>
        private UserDAO dao = new UserDAO();

        /// <summary>
        /// 分页获取用户信息
        /// </summary>
        /// <param name="start">第几行开始</param>
        /// <param name="length">获取几条</param>
        /// <returns></returns>
        [HttpGet]
        [ApiGroup(ApiGroupNames.EF)]
        public ActionResult<IEnumerable<UserModel>> Get(int start = 0, int length = 10)
        {
            //加上ReadUncommitted级别的事务，相当于WITH ( NOLOCK )，不锁表
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                return dao.FindAll().OrderBy(a => a.Id).Skip(0).Take(10).Select(a => new UserModel()
                {
                    Id = a.Id,
                    Name = a.Name,
                    Age = a.Age,
                    Sex = a.Sex,
                    Birthday = a.Birthday,
                    Address = a.Address,
                    Height = a.Height,
                    Money = a.Money
                }).ToList();
            }
        }

        /// <summary>
        /// 根据id获取用户信息
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ApiGroup(ApiGroupNames.EF)]
        public ActionResult<UserModel> GetById(int id)
        {
            //加上ReadUncommitted级别的事务，相当于WITH ( NOLOCK )，不锁表
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                return dao.FindBy(a => a.Id == id).Select(a => new UserModel()
                {
                    Id = a.Id,
                    Name = a.Name,
                    Age = a.Age,
                    Sex = a.Sex,
                    Birthday = a.Birthday,
                    Address = a.Address,
                    Height = a.Height,
                    Money = a.Money
                }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 新增用户信息
        /// </summary>
        /// <param name="model">值</param>
        [HttpPost]
        [ApiGroup(ApiGroupNames.EF)]
        public bool Insert([FromBody] UserModel model)
        {
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var result = dao.Create(new User()
                {
                    Name = model.Name,
                    Age = model.Age,
                    Sex = model.Sex,
                    Birthday = model.Birthday,
                    Address = model.Address,
                    Height = model.Height,
                    Money = model.Money
                });
                t.Complete();
                return result;
            }
        }

        /// <summary>
        /// 批量新增用户信息
        /// </summary>
        /// <param name="models">值</param>
        [HttpPost]
        [ApiGroup(ApiGroupNames.EF)]
        public bool BatchInsert([FromBody] List<UserModel> models)
        {
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var result = dao.AddRange(models.Select(a => new User()
                {
                    Name = a.Name,
                    Age = a.Age,
                    Sex = a.Sex,
                    Birthday = a.Birthday,
                    Address = a.Address,
                    Height = a.Height,
                    Money = a.Money
                }).ToList());
                t.Complete();
                return result;
            }
        }

        /// <summary>
        /// 修改用户名称
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="model">值</param>
        [HttpPut("{id}")]
        [ApiGroup(ApiGroupNames.EF)]
        public bool Update(int id, [FromBody] UserModel model)
        {
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var user = new User();
                user.Id = id;
                user.Name = model.Name;
                var result = dao.Update(user, new string[] {
                    user.PropertyNames.Name
                });
                //t.Complete();
                return result;
            }
        }

        /// <summary>
        /// 根据id删除用户信息
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        [ApiGroup(ApiGroupNames.EF)]
        public bool Delete(int id)
        {
            using (var t = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var result = dao.Delete(id);
                t.Complete();
                return result;
            }
        }
    }
}