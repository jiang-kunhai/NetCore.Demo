using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NetCore.API.Common.Filters;
using NetCore.API.Extensions.Swagger;
using NetCore.API.Models;
using NetCore.Common.Helpers;
using NetCore.Common.JsonConverter;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace NetCore.API {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
            ConfigHelper._configuration = Configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            //appsettings.json 文件的 JwtAuth 部分绑定到的配置实例在依赖关系注入 (DI) 容器中注册
            services.Configure<JwtAuthConfig>(Configuration.GetSection(JwtAuthConfig.ConfigName));
            // 接口使用单一实例服务生存期在 DI 中注册
            services.AddSingleton<IJwtAuthConfig>(sp => sp.GetRequiredService<IOptions<JwtAuthConfig>>().Value);

            services.AddControllers()//全局配置Json序列化处理
                .AddJsonOptions(options => {
                    //格式策略，目前内置的仅有一种策略CamelCase
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    //添加事件格式化类
                    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                    options.JsonSerializerOptions.Converters.Add(new DateTimeNullConverter());
                })
                //指定版本
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            #region CORS

            // 先注入服务，声明策略，然后再下边app中配置开启中间件
            services.AddCors(c => {
                // ↓↓↓↓↓↓↓注意正式环境不要使用这种全开放的处理↓↓↓↓↓↓↓↓↓↓
                c.AddPolicy("AllRequests", policy => {
                    policy
                    .SetIsOriginAllowed(_ => true)
                    //.AllowAnyOrigin()//允许任何源，3.0直接设置会报错
                    .AllowAnyMethod()//允许任何方式
                    .AllowAnyHeader()//允许任何头
                    .AllowCredentials();//允许cookie
                });
                // ↑↑↑↑↑↑↑注意正式环境不要使用这种全开放的处理↑↑↑↑↑↑↑↑↑↑

                // 一般采用这种方法
                c.AddPolicy("LimitRequests", policy => {
                    policy
                    // 支持多个域名端口，注意端口号后不要带/斜杆：比如localhost:8000/，是错的
                    .WithOrigins("http://127.0.0.1:1818", "http://localhost:8080", "http://localhost:8021", "http://localhost:8081", "http://localhost:1818")
                    .AllowAnyHeader() // Ensures that the policy allows any header.
                    .AllowAnyMethod();
                });
            });

            #endregion CORS

            //角色、策略授权https://www.cnblogs.com/wei325/p/15575141.html
            #region Jwt验证-https://www.cnblogs.com/7tiny/archive/2019/06/13/11012035.html

            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                //获取配置
                JwtAuthConfig jwtAuthConfig = ConfigHelper.GetConfig<JwtAuthConfig>(JwtAuthConfig.ConfigName);
                //验证设置
                options.TokenValidationParameters = new TokenValidationParameters {
                    ValidateIssuer = true,//是否验证发布者
                    ValidIssuer = jwtAuthConfig.Issuer,//发布者名称
                    ValidateAudience = true,//是否验证订阅者
                    ValidAudience = jwtAuthConfig.Audience,//订阅者名称
                    ValidateLifetime = true,//是否验证令牌有效期
                    ClockSkew = TimeSpan.FromMinutes(30),// 每次颁发令牌，令牌有效时间
                    ValidateIssuerSigningKey = true,//是否验证安全密钥
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtAuthConfig.SecurityKey))//安全密钥
                };
                //权限验证失败后触发的事件
                options.Events = new JwtBearerEvents {
                    OnChallenge = context => {
                        //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
                        context.HandleResponse();
                        //自定义自己想要返回的数据结果
                        var payload = JsonHelper.Serialize(new { Code = "401", Msg = "很抱歉，您无权访问该接口！" });
                        //自定义返回的数据类型
                        context.Response.ContentType = "application/json";
                        //自定义返回状态码，默认为401
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        //输出Json数据结果
                        context.Response.WriteAsync(payload);
                        return Task.FromResult(0);
                    }
                };
            });

            #endregion Jwt验证-https://www.cnblogs.com/7tiny/archive/2019/06/13/11012035.html

            #region Swagger https://www.cnblogs.com/cool-net/p/15655036.html

            services.AddSwaggerGen(app => {
                //设置文档信息
                app.SwaggerDoc("v1", new OpenApiInfo {
                    Title = "ApiHelp",
                    Version = "v1",
                    Description = "框架集合",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact {
                        Name = "联系信息",
                        Email = "Email@qq.com",
                        Url = new Uri("https://example.com/contact"),
                    },
                    License = new OpenApiLicense {
                        Name = "许可证",
                        Url = new Uri("https://example.com/license"),
                    }
                });

                // 反射用于生成与 Web API 项目相匹配的 XML 文件名
                var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                app.IncludeXmlComments(xmlPath);

                //添加header验证信息
                app.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme() {
                    Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
                    Name = "Authorization",//jwt默认的参数名称
                    In = ParameterLocation.Header,//jwt默认存放Authorization信息的位置(请求头中)
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                //添加授权参数
                app.AddSecurityRequirement(new OpenApiSecurityRequirement{
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });

                // 遍历ApiGroupNames所有枚举值生成接口文档，Skip(1)是因为Enum第一个FieldInfo是内置的一个Int值
                typeof(ApiGroupNames).GetFields().Skip(1).ToList().ForEach(f => {
                    //获取枚举值上的特性
                    var info = f.GetCustomAttributes(typeof(GroupInfoAttribute), false).OfType<GroupInfoAttribute>().FirstOrDefault();
                    app.SwaggerDoc(f.Name, new OpenApiInfo {
                        Title = info?.Title,
                        Version = info?.Version,
                        Description = info?.Description
                    });
                });
                // 没有特性的接口分到NoGroup上
                app.SwaggerDoc("NoGroup", new OpenApiInfo {
                    Title = "无分组"
                });
                // 判断接口归于哪个分组
                app.DocInclusionPredicate((docName, apiDescription) => {
                    if (docName == "NoGroup") {
                        // 当分组为NoGroup时，只要没加特性的接口都属于这个组
                        return string.IsNullOrEmpty(apiDescription.GroupName);
                    }
                    else {
                        return apiDescription.GroupName == docName;
                    }
                });
            });

            #endregion Swagger
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                //异常处理-开发人员异常页
                //app.UseDeveloperExceptionPage();
                //添加跨域
                app.UseCors("AllRequests");

                //Swagger中间件
                app.UseSwagger();
                app.UseSwaggerUI(options => {
                    // 遍历ApiGroupNames所有枚举值生成接口文档
                    typeof(ApiGroupNames).GetFields().Skip(1).ToList().ForEach(f => {
                        //获取枚举值上的特性
                        var info = f.GetCustomAttributes(typeof(GroupInfoAttribute), false).OfType<GroupInfoAttribute>().FirstOrDefault();
                        options.SwaggerEndpoint($"/swagger/{f.Name}/swagger.json", info != null ? info.Title : f.Name);
                    });
                    options.SwaggerEndpoint("/swagger/NoGroup/swagger.json", "无分组");
                    options.RoutePrefix = string.Empty;//清除swagger路由前缀
                });
            }
            else {
                //添加跨域
                app.UseCors("LimitRequests");
            }

            //异常处理-返回json
            app.UseExceptionHandler(a => a.Run(async context => {
                var feature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = feature.Error;
                var result = JsonHelper.Serialize(new {
                    code = context.Response.StatusCode,
                    error = exception.Message
                });
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(result);
            }));

            //HTTPS 重定向中间件将 HTTP 请求重定向到 HTTPS。
            app.UseHttpsRedirection();

            //向中间件管道添加路由匹配。 此中间件会查看应用中定义的终结点集（UseEndpoints），并根据请求选择最佳匹配。
            app.UseRouting();

            //身份验证中间件尝试对用户进行身份验证，然后才会允许用户访问安全资源
            //读取客户端中的身份标识(Cookie,Token等)并解析出来，存储到 HttpContext.User 中。
            app.UseAuthentication();

            //用于授权用户访问安全资源的授权中间件
            //判断当前访问 Endpoint(Controller或Action)是否使用了[Authorize]以及配置角色或策略，然后校验 Cookie 或 Token 是否有效。
            app.UseAuthorization();

            // 添加自定义授权中间件
            app.UseMiddleware<JwtAuthorizationFilter>();

            //向中间件管道添加终结点执行。 它会运行与所选终结点关联的委托。
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}