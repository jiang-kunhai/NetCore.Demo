﻿namespace NetCore.API.Models
{
    /// <summary>
    /// Jwt配置
    /// </summary>
    public class JwtAuthConfig : IJwtAuthConfig
    {
        /// <summary>
        /// 配置文件名称
        /// </summary>
        public const string ConfigName = "JwtAuth";

        /// <summary>
        /// jwt签发者,非必须
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// jwt的接收方，非必须
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 安全密钥
        /// </summary>
        public string SecurityKey { get; set; }

        /// <summary>
        /// Web端过期时间
        /// </summary>
        public double WebExp { get; set; }

        /// <summary>
        /// 移动端过期时间
        /// </summary>
        public double AppExp { get; set; }

        /// <summary>
        /// 小程序过期时间
        /// </summary>
        public double MiniProgramExp { get; set; }

        /// <summary>
        /// 其他端过期时间
        /// </summary>
        public double OtherExp { get; set; }
    }

    public interface IJwtAuthConfig
    {
        /// <summary>
        /// jwt签发者,非必须
        /// </summary>
        string Issuer { get; set; }

        /// <summary>
        /// jwt的接收方，非必须
        /// </summary>
        string Audience { get; set; }

        /// <summary>
        /// 安全密钥
        /// </summary>
        string SecurityKey { get; set; }

        /// <summary>
        /// Web端过期时间
        /// </summary>
        double WebExp { get; set; }

        /// <summary>
        /// 移动端过期时间
        /// </summary>
        double AppExp { get; set; }

        /// <summary>
        /// 小程序过期时间
        /// </summary>
        double MiniProgramExp { get; set; }

        /// <summary>
        /// 其他端过期时间
        /// </summary>
        double OtherExp { get; set; }
    }
}