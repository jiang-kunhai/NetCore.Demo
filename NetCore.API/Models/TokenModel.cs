﻿namespace NetCore.API.Models
{
    public class TokenModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Uid { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string Uname { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 身份
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// 令牌类型 Web 网页、 App 移动、 MiniProgram 小程序、 Other 其他
        /// </summary>
        public string TokenType { get; set; }
    }
}