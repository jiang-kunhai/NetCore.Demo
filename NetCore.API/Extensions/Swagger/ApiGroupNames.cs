﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.API.Extensions.Swagger {
    /// <summary>
    /// 接口分组枚举
    /// </summary>
    public enum ApiGroupNames {

        [GroupInfo(Title = "登录认证", Description = "Jwt认证", Version = "v1")]
        Login,

        [GroupInfo(Title = "EF", Description = "EF操作SqlServer", Version = "v1")]
        EF,
    }
}
