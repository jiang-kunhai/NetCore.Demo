﻿using Microsoft.AspNetCore.Http;
using NetCore.API.Common.Helpers;
using NetCore.API.Models;
using System.Threading.Tasks;

namespace NetCore.API.Common.Filters
{
    /// <summary>
    /// 自定义授权中间件
    /// </summary>
    public class JwtAuthorizationFilter
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="next"></param>
        public JwtAuthorizationFilter(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public Task Invoke(HttpContext httpContext)
        {
            //检测是否包含'Authorization'请求头，如果不包含则直接放行
            if (!httpContext.Request.Headers.ContainsKey("Authorization"))
            {
                return _next(httpContext);
            }
            var tokenHeader = httpContext.Request.Headers["Authorization"];

            try
            {
                tokenHeader = tokenHeader.ToString().Substring("Bearer ".Length).Trim();

                //获取授权信息
                TokenModel tm = JwtHelper.SerializeJWT(tokenHeader);

                //验证令牌黑名单
            }
            catch
            {
            }

            return _next(httpContext);
        }
    }
}