using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCore.Common.Helpers;
using NetCore.Common.JsonConverter;
using NetCore.Common.Models;
using System;
using System.Text.Json;

namespace NetCore.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //菜单加载初始化
            Common.Helpers.MenuInfoHelper.Init();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //IDistributedCache 实现用作会话后备存储
            services.AddDistributedMemoryCache();

            //添加会话
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            //AddMvc()等于AddControllersWithViews() 加 AddRazorPages()
            services.AddMvc()
                //全局配置Json序列化处理
                .AddJsonOptions(options =>
                {
                    //格式策略，目前内置的仅有一种策略CamelCase
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    //添加事件格式化类
                    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                    options.JsonSerializerOptions.Converters.Add(new DateTimeNullConverter());
                })
                //指定版本
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //异常处理-开发人员异常页
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                //异常处理-返回一个错误页
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //异常处理-返回自定义json
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var feature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = feature.Error;
                var result = JsonHelper.Serialize(new JsonResultModel
                {
                    IsOk = false,
                    Msg = exception.Message,
                    Data = context.Response.StatusCode
                });
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(result);
            }));

            //HTTPS 重定向中间件将 HTTP 请求重定向到 HTTPS。
            app.UseHttpsRedirection();

            //静态文件中间件返回静态文件，并简化进一步请求处理。
            app.UseStaticFiles();

            // Cookie 策略中间件使应用符合欧盟一般数据保护条例(GDPR) 规定。
            app.UseCookiePolicy();

            //向中间件管道添加路由匹配。 此中间件会查看应用中定义的终结点集（UseEndpoints），并根据请求选择最佳匹配。
            app.UseRouting();

            //身份验证中间件尝试对用户进行身份验证，然后才会允许用户访问安全资源
            app.UseAuthorization();

            //会话中间件建立和维护会话状态。
            //如果应用使用会话状态，请在 Cookie 策略中间件之后和 MVC 中间件之前调用会话中间件。
            app.UseSession();

            //向中间件管道添加终结点执行。 它会运行与所选终结点关联的委托。
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                //区域路由设置
                endpoints.MapAreaControllerRoute(
                    name: "JSPlugin_default",
                    areaName: "JSPlugin",
                    pattern: "{area:exists}/{controller}/{action}/{id?}");
                endpoints.MapAreaControllerRoute(
                    name: "Tool_default",
                    areaName: "Tool",
                    pattern: "{area:exists}/{controller}/{action}/{id?}");
            });
        }
    }
}