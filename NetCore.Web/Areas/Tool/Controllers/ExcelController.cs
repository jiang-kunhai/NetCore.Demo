﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetCore.Common.Helpers;
using NetCore.Common.Models;
using NetCore.Web.Common.CustomAttribute;
using NetCore.Web.Common.Helpers;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NetCore.Web.Areas.Tool.Controllers
{
    public class ExcelController : BaseController
    {
        [MenuInfo(Title = "Excel操作", Sort = 1)]
        public IActionResult Index()
        {
            return View();
        }

        #region Npoi

        /// <summary>
        /// 导出模板
        /// </summary>
        /// <returns></returns>
        public IActionResult ExportNpoi()
        {
            try
            {
                NpoiHepler excelHepler = new NpoiHepler();
                int rowIndex = 0;
                int colIndex = 0;

                excelHepler.AddCell(rowIndex, colIndex++, "1位小数", NpoiStyleType.Title);
                excelHepler.AddCell(rowIndex, colIndex++, "日期时间", NpoiStyleType.Title);
                excelHepler.AddCell(rowIndex, colIndex++, "下拉框", NpoiStyleType.Title);
                excelHepler.AddCell(rowIndex, colIndex++, "合并2*2单元格", NpoiStyleType.Title, 1, 1);

                rowIndex++;
                colIndex = 0;
                excelHepler.AddCell(rowIndex, colIndex++, 100, NpoiStyleType.OneDecimalPlaces);
                excelHepler.AddCell(rowIndex, colIndex++, DateTime.Now, NpoiStyleType.DateTime);
                excelHepler.AddSelectCol(rowIndex, colIndex++, new string[] { "下拉框值1", "下拉框值2" });

                var fileDownloadName = $"示例.xlsx";
                return File(excelHepler.GetByte(), excelHepler.ContentType, fileDownloadName);
            }
            catch (Exception ex)
            {
                Response.ContentType = "text/plain;charset=utf-8";
                Response.WriteAsync("导出失败，请联系管理员，错误描述：" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public IActionResult UploadNpoi(IFormFile file)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                #region 验证文件格式

                if (Request.Form.Files.Count == 0)
                {
                    result.IsOk = false;
                    result.Msg = "请选择上传文件";
                    return Json(result);
                }
                string fileExt = System.IO.Path.GetExtension(file.FileName).ToLower();
                if (fileExt != ".xlsx")
                {
                    result.IsOk = false;
                    result.Msg = "上传的文件类型必须为.xlsx类型";
                    return Json(result);
                }

                #endregion 验证文件格式

                #region 获取文件数据

                IWorkbook wk = null;
                if (fileExt == ".xls")
                {
                    //把xls文件中的数据写入wk中
                    wk = new NPOI.HSSF.UserModel.HSSFWorkbook(file.OpenReadStream());
                }
                else
                {
                    //把xlsx文件中的数据写入wk中
                    wk = new NPOI.XSSF.UserModel.XSSFWorkbook(file.OpenReadStream());
                }
                //读取当前表数据
                ISheet sheet = wk.GetSheetAt(0);
                if (sheet.LastRowNum < 1)
                {
                    result.IsOk = false;
                    result.Msg = "上传的Excel无内容";
                    return Json(result);
                }
                List<List<string>> dataList = new List<List<string>>();
                for (int rowNum = 0; rowNum < sheet.LastRowNum; rowNum++)
                {
                    var row = sheet.GetRow(rowNum);
                    if (row != null)
                    {
                        List<string> rowList = new List<string>();
                        for (int colNum = 0; colNum < row.LastCellNum; colNum++)
                        {
                            var cell = row.GetCell(colNum);
                            rowList.Add(NpoiHepler.GetCellValue(cell));
                        }
                        dataList.Add(rowList);
                    }
                }

                #endregion 获取文件数据

                result.Data = dataList;
                return Json(result);
            }
            catch (Exception ex)
            {
                result.IsOk = false;
                result.Msg = "导入出错:" + ex.Message;
            }

            return Json(result);
        }

        #endregion Npoi

        #region EPPlus

        /// <summary>
        /// 导出模板
        /// </summary>
        /// <returns></returns>
        public IActionResult ExportEPPlus()
        {
            try
            {
                //指定EPPlus使用非商业证书,否则报错
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.Add("Sheet1");

                    #region 插入数据，坐标从1开始

                    var list = RandomUserHelper.GetList().Take(10).ToList();
                    int rowIndex = 1;
                    for (int n = 0; n < list.Count; n++)
                    {
                        var data = list[n];
                        int colIndex = 1;
                        AddCells(workSheet, rowIndex, colIndex, data.Id);
                        colIndex++;
                        AddCells(workSheet, rowIndex, colIndex, data.Name);
                        colIndex++;
                        AddCells(workSheet, rowIndex, colIndex, data.Birthday);
                        colIndex++;
                        AddCells(workSheet, rowIndex, colIndex, data.Age);
                        colIndex++;
                        AddCells(workSheet, rowIndex, colIndex, data.Sex);
                        colIndex++;
                        AddCells(workSheet, rowIndex, colIndex, data.Height);

                        rowIndex++;
                    }

                    AddCells(workSheet, rowIndex, 1, "合并单元格", true, 2, 2);

                    #endregion 插入数据，坐标从1开始

                    #region 列宽度设置

                    //自动宽度，填完内容后调用
                    workSheet.Cells[workSheet.Dimension.Address].AutoFitColumns();

                    #endregion 列宽度设置

                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Epplus模板.xlsx");
                }
            }
            catch (Exception ex)
            {
                Response.ContentType = "text/plain;charset=utf-8";
                Response.WriteAsync("导出失败，请联系管理员，错误描述：" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 插入单元格数据
        /// </summary>
        /// <param name="workSheet">ExcelWorksheet 对象</param>
        /// <param name="rowIndex">行</param>
        /// <param name="colIndex">列</param>
        /// <param name="value">值</param>
        /// <param name="border">是否显示边框</param>
        /// <param name="mergeColumns">合并的几列</param>
        /// <param name="mergeRow">合并的几行</param>
        public void AddCells(ExcelWorksheet workSheet, int rowIndex, int colIndex, object value, bool border = true, int mergeColumns = 0, int mergeRow = 0)
        {
            switch (value.GetType().ToString())
            {
                case "System.String"://字符串类型
                    workSheet.Cells[rowIndex, colIndex].Value = value;
                    break;

                case "System.DateTime"://日期类型
                    workSheet.Cells[rowIndex, colIndex].Style.Numberformat.Format = "yyyy-mm-d h:mm:ss";
                    workSheet.Cells[rowIndex, colIndex].Value = value;
                    break;

                case "System.Boolean"://布尔型
                    workSheet.Cells[rowIndex, colIndex].Value = (bool)value ? "是" : "否";
                    break;

                case "System.Int16"://整型
                case "System.Int32":
                case "System.Int64":
                case "System.Byte":
                    workSheet.Cells[rowIndex, colIndex].Style.Numberformat.Format = "#,##0";
                    workSheet.Cells[rowIndex, colIndex].Value = value;
                    break;

                case "System.Decimal"://浮点型
                case "System.Double":
                    workSheet.Cells[rowIndex, colIndex].Style.Numberformat.Format = "#,##0.00";
                    workSheet.Cells[rowIndex, colIndex].Value = value;
                    break;

                case "System.DBNull"://空值处理
                    workSheet.Cells[rowIndex, colIndex].Value = "";
                    break;

                default:
                    workSheet.Cells[rowIndex, colIndex].Value = "";
                    break;
            }

            //合并单元格
            if (mergeColumns > 0 || mergeRow > 0)
            {
                workSheet.Cells[rowIndex, colIndex, rowIndex + mergeRow, colIndex + mergeColumns].Merge = true;
                if (border)
                {
                    workSheet.Cells[rowIndex, colIndex, rowIndex + mergeRow, colIndex + mergeColumns].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowIndex, colIndex, rowIndex + mergeRow, colIndex + mergeColumns].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowIndex, colIndex, rowIndex + mergeRow, colIndex + mergeColumns].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowIndex, colIndex, rowIndex + mergeRow, colIndex + mergeColumns].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                }
            }
            else if (border)
            {
                workSheet.Cells[rowIndex, colIndex].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[rowIndex, colIndex].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[rowIndex, colIndex].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[rowIndex, colIndex].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            }
        }

        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public IActionResult UploadEPPlus(IFormFile file)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                #region 验证文件格式

                if (Request.Form.Files.Count == 0)
                {
                    result.IsOk = false;
                    result.Msg = "请选择上传文件";
                    return Json(result);
                }
                string fileExt = System.IO.Path.GetExtension(file.FileName).ToLower();
                if (fileExt != ".xls" && fileExt != ".xlsx")
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "上传的文件类型必须为.xls或.xlsx类型"
                    });
                }

                #endregion 验证文件格式

                List<List<string>> dataList = new List<List<string>>();

                #region 获取文件数据

                //指定EPPlus使用非商业证书,否则报错
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file.OpenReadStream()))
                {
                    if (package.Workbook.Worksheets.Count == 0)
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = "上传的文件没有工作表！"
                        });
                    }
                    var sheet = package.Workbook.Worksheets[0];
                    if (sheet.Dimension == null)
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = "工作表内容为空！"
                        });
                    }

                    var columnCount = sheet.Dimension.End.Column;//获取worksheet的列
                    var rowCount = sheet.Dimension.End.Row;//获取worksheet的行数
                    for (int y = 1; y <= rowCount; y++)
                    {
                        List<string> rowList = new List<string>();
                        for (int x = 1; x <= columnCount; x++)
                        {
                            if (sheet.Cells[y, x].Value == null)
                            {
                                rowList.Add("");
                            }
                            else
                            {
                                rowList.Add(sheet.Cells[y, x].Value.ToString());
                            }
                        }
                        dataList.Add(rowList);
                    }
                }

                #endregion 获取文件数据

                result.Data = dataList;
                return Json(result);
            }
            catch (Exception ex)
            {
                result.IsOk = false;
                result.Msg = "导入出错:" + ex.Message;
            }

            return Json(result);
        }

        #endregion EPPlus
    }
}