﻿using Microsoft.AspNetCore.Mvc;

namespace NetCore.Web.Areas.Tool.Controllers
{
    [Area("Tool")]
    [Route("Tool/[controller]/[action]")]
    public class BaseController : Controller
    {
    }
}