﻿using Microsoft.AspNetCore.Mvc;
using NetCore.Web.Common.CustomAttribute;
using NetCore.Web.Areas.JSPlugin.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.Web.Areas.JSPlugin.Controllers {
    public class ValidateController : BaseController {


        [MenuInfo(Title = "表单验证", Sort = 1)]
        public IActionResult Index() {
            return View();
        }
    }
}
