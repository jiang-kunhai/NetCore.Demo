﻿using Microsoft.AspNetCore.Mvc;
using NetCore.Web.Common.CustomAttribute;

namespace NetCore.Web.Areas.JSPlugin.Controllers
{
    public class MindMapController : BaseController
    {
        [MenuInfo(Title = "思维脑图", Sort = 1)]
        public IActionResult Index()
        {
            return View();
        }
    }
}