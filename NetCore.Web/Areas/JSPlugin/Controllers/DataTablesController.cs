﻿using Microsoft.AspNetCore.Mvc;
using NetCore.Common.Helpers;
using NetCore.Web.Common.CustomAttribute;
using System.Linq;

namespace NetCore.Web.Areas.JSPlugin.Controllers
{
    public class DataTablesController : BaseController
    {
        [MenuInfo(Title = "表格", Sort = 1)]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Get(string name, int draw = 1, int start = 0, int length = 10)
        {
            var data = RandomUserHelper.GetList();
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(a => a.Name.Contains(name)).ToList();
            }
            var count = data.Count();
            var resultData = data.Skip(start).Take(length).ToList();
            var result = new
            {
                draw,
                recordsTotal = count,
                recordsFiltered = count,
                data = resultData
            };
            return Json(result);
        }
    }
}