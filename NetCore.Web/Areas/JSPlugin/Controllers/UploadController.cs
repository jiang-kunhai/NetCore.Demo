﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NetCore.Web.Common.CustomAttribute;
using NetCore.Web.Areas.JSPlugin.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NetCore.Web.Areas.JSPlugin.Controllers {
    public class UploadController : BaseController {

        IWebHostEnvironment _webHostEnvironment;
        public UploadController(IWebHostEnvironment webHostEnvironment) {
            _webHostEnvironment = webHostEnvironment;
        }


        [MenuInfo(Title = "文件上传", Sort = 1)]
        public IActionResult Index() {
            return View();
        }

        public IActionResult UploadFile(List<IFormFile> files) {

            //asp.net使用Server.MapPath("~/")
            string content_path = _webHostEnvironment.ContentRootPath;//E:\代码\NetCore.Demo\NetCore.Web
            string web_path = _webHostEnvironment.WebRootPath;//E:\代码\NetCore.Demo\NetCore.Web\wwwroot

            if (files?.Count > 0) {
                return Json(files[0].FileName);
            }
            return Json("无文件");
        }
    }
}
