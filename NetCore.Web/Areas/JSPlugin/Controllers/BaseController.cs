﻿using Microsoft.AspNetCore.Mvc;

namespace NetCore.Web.Areas.JSPlugin.Controllers
{
    [Area("JSPlugin")]
    [Route("JSPlugin/[controller]/[action]")]
    public class BaseController : Controller
    {
    }
}