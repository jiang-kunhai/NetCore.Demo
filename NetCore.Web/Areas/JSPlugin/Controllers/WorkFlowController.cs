﻿using Microsoft.AspNetCore.Mvc;
using NetCore.Web.Common.CustomAttribute;

namespace NetCore.Web.Areas.JSPlugin.Controllers
{
    public class WorkFlowController : BaseController
    {
        [MenuInfo(Title = "工作流", Sort = 1)]
        public IActionResult Index()
        {
            return View();
        }
    }
}