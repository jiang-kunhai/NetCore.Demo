{
	"sProcessing":   "处理中...",
	"sLengthMenu":   "每页 _MENU_ 条",
	"sZeroRecords":  "没有匹配结果",
	"sInfo":         "(_START_ - _END_) 共 _TOTAL_ 条",
	"sInfoEmpty":    "共 0 条",
	"sInfoFiltered": "(由 _MAX_ 条结果过滤)",
	"sInfoPostFix":  "",
	"sSearch":       "搜索:",
	"sUrl":          "",
	"sEmptyTable":     "未找到数据",
	"sLoadingRecords": "载入中...",
	"sInfoThousands":  ",",
	"oPaginate": {
		"sFirst":    "首页",
		"sPrevious": "上页",
		"sNext":     "下页",
		"sLast":     "末页"
	},
	"oAria": {
		"sSortAscending":  ": 以升序排列此列",
		"sSortDescending": ": 以降序排列此列"
	}
}
