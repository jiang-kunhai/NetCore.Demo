﻿//显示加载遮罩
function ShowProcess(timeout) {
    $('#modelProcess').modal({ backdrop: 'static', keyboard: false });

    if (timeout) {
        setTimeout(function () {
            $("#modelProcess").modal("hide");
        }, 1000 * timeout);
    }
}
//关闭加载遮罩
function HideProcess() {
    setTimeout(function () {
        $("#modelProcess").modal("hide");
    }, 500);
}
//消息提示
function ShowMessage(msg, timeout) {
    $("#divModelMsgContent").html(msg);
    $('#modelMessage').modal({ backdrop: 'static' });
    $("#modelMessage").modal("show");

    if (timeout) {
        setTimeout(function () {
            $("#modelMessage").modal("hide");
        }, 1000 * timeout);
    }
}