﻿using NetCore.Web.Common.CustomAttribute;
using NetCore.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NetCore.Web.Common.Helpers
{
    /// <summary>
    /// 菜单信息特性帮助类
    /// </summary>
    public class MenuInfoHelper
    {
        private static List<MenuInfoModel> _menuInfoList = new List<MenuInfoModel>();

        /// <summary>
        /// 初始化
        /// </summary>
        public static void Init()
        {
            var assembly = typeof(Startup).Assembly.GetTypes().AsEnumerable()
                .Where(type => typeof(Microsoft.AspNetCore.Mvc.ControllerBase).IsAssignableFrom(type)).ToList();

            assembly.ForEach(r =>
            {
                foreach (var methodInfo in r.GetMethods())
                {
                    foreach (Attribute attribute in methodInfo.GetCustomAttributes())
                    {
                        if (attribute is MenuInfoAttribute menuInfoAuthorize)
                        {
                            MenuInfoModel model = new MenuInfoModel();
                            model.Title = menuInfoAuthorize.GetType().GetProperty("Title").GetValue(menuInfoAuthorize)?.ToString();
                            model.Sort = (int)menuInfoAuthorize.GetType().GetProperty("Sort").GetValue(menuInfoAuthorize);
                            var fullName = methodInfo.DeclaringType.FullName;
                            if (fullName.StartsWith("NetCore.Web.Areas"))
                            {
                                model.Areas = fullName.Substring(0, fullName.IndexOf(".Controllers"))
                                                      .Replace("NetCore.Web.Areas.", "");
                            }
                            model.Controller = methodInfo.DeclaringType.Name.Replace("Controller", "");
                            model.Action = methodInfo.Name;
                            _menuInfoList.Add(model);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <returns></returns>
        public static List<MenuInfoModel> GetMenuInfos()
        {
            return _menuInfoList;
        }
    }
}