﻿using System;

namespace NetCore.Web.Common.CustomAttribute
{
    /// <summary>
    /// 菜单信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class MenuInfoAttribute : Attribute
    {
        /// <summary>
        /// 菜单标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; } = 999;
    }
}